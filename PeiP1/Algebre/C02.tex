\input{preamble}

\begin{document}
	\includecourse

	\section{Les ensembles}%
	\label{sec:les_ensembles}

	\setcounter{subsection}{2}
	\subsection{Produit cartésien d'ensembles}%
	\label{sub:produit_cartesien_d_ensembles}

	\begin{rmq}
		\begin{itemize}
			\item Soit $E$ un ensemble.
			\begin{itemize}
				\item $\exists \times E$ se note $E^2$.
				\item $(x, y) \in E^2 \iff x\in E \et y \in E$.
			\end{itemize}
		\end{itemize}
	\end{rmq}

	\section{Ensemble des parties de $E$}%
	\label{sec:ensemble_des_parties_de_e_}

	\subsection{Définition}%
	\label{sub:definition}

	\begin{prop}
		\textbf{Proposition 1}

		Soient $E$ un ensemble et  $n$ un entier naturel.

		Si $E$ contient $n$ éléments alors $\mathscr{P}(E)$ contient  $2^{n}$ éléments.
	\end{prop}

	\begin{demo}
		$E$ est un ensemble contenant $n$ éléments.

		$ \mathscr{P}(E)$ est constitué des parties de $E$ ayant :
		\begin{itemize}
			\item 0 élément : il y en a \cobi{0}{n}
			\item 1 élément : il y en a \cobi{1}{n}
			\item $k$ éléments : il y en a \cobi{k}{n}
			\item éléments : il y a en a \cobi{n}{n}
		\end{itemize}

		\Finalement le nombre de parties de $E$ est :
		\[
			\sum_{k=0}^{n} \cobi{k}{n}=\sum_{k=0}^{n} \cobi{k}{n}1^k \times 1^{n-k}=2^{n}
		\]
	\end{demo}

	\begin{exo}
		Soit $E = \left\{ a, b, c \right\}$. Déterminer $ \mathscr{P}(E)$.

		$ \mathscr{P}(E) = \big\{\emptyset, \left\{ a \right\}, \left\{ b \right\} ,
		\left\{ c \right\} , \left\{ a,b \right\} , \left\{ a,c \right\} , \left\{ b,c
	\right\} , \left\{ a,b,c \right\}  \big\} $
	\end{exo}

	\begin{prop}
		Soit $E$ un ensemble.

		L'inclusion est transitive en effet : $\forall A, B,C \in \mathscr{P}(E), \left( A
		\subset B \et B \subset C \right) \implies A \subset C$
	\end{prop}

	\begin{demo}
		Soit $E$ un ensemble.

		La réflexivité et l'antisymétrie sont évidentes.

		Montrons la transitivité

		Soient $A, B, C \in \mathscr{P}(E)$

		Supposons que $A \subset B \et B \subset C$

		\begin{itemize}
			\item Soit $x \in A$

			$A \subset B$

			Donc $x \in B$.

			Or $B \subset C$.

			Donc $x \in C$.
		\end{itemize}

		On a montré que : $\forall x \in A, x \in C$

		Donc $A \subset C$.
	\end{demo}

	\setcounter{subsection}{2}
	\subsection{Opérations dans $ \mathscr{P}(E)$}%
	\label{sub:operations_dans_p_e_}

	\def\firstcircle{(0,0) circle (1.5cm)}
	\def\secondcircle{(0:2cm) circle (1.5cm)}

	\colorlet{circle edge}{blue!50}
	\colorlet{circle area}{blue!20}

	\tikzset{filled/.style={fill=circle area, draw=circle edge, thick},
	outline/.style={draw=circle edge, thick}}

	\begin{figure}[H]
		\center
		\begin{tikzpicture}
			\begin{scope}
				\clip \firstcircle;
				\fill[filled] \secondcircle;
			\end{scope}
			\draw[outline] \firstcircle node {$A$};
			\draw[outline] \secondcircle node {$B$};
			\node[anchor=south] at (current bounding box.north) {$A \cap B$};
		\end{tikzpicture}
		\hspace{1cm}
		\begin{tikzpicture}
			\draw[filled] \firstcircle node {$A$}
						  \secondcircle node {$B$};
			\node[anchor=south] at (current bounding box.north) {$A \cup B$};
		\end{tikzpicture}

		\begin{tikzpicture}
			\begin{scope}
				\clip \firstcircle;
				\draw[filled, even odd rule] \firstcircle node {$A$}
											 \secondcircle;
			\end{scope}
			\draw[outline] \firstcircle
						   \secondcircle node {$B$};
			\node[anchor=south] at (current bounding box.north) {$A \setminus B$};
		\end{tikzpicture}
		\hspace{1cm}
		\begin{tikzpicture}
			\draw[filled, even odd rule] \firstcircle node {$A$}
										 \secondcircle node{$B$};
			\node[anchor=south] at (current bounding box.north) {$A \Delta B$};
		\end{tikzpicture}
	\end{figure}

	\begin{demo}
		\textbf{Commutativité} :

		Soient $E$ un ensemble et $A$ et $B$ deux parties de $E$.

		Soit  $x \in E$.

		\begin{align*}
			x \in A \cap B \iff& x \in A \et x \in B \\
			\iff& x \in B \et x \in A \\
			\iff& x \in B \cap A
		\end{align*}
		Donc $A \cap B = B \cap A$

		\bigskip
		\textbf{Associativité} :

		Soient $E$ un ensemble et $A$, $B$ et $C$ trois parties de $E$.

		Soit $x \in E$.

		\begin{align*}
			x \in (A \cap B) \cap C \iff& x \in A \cap B \et x \in C \\
			\iff& (x \in A \et x \in B) \et x \in C \\
			\iff&  x \in A \et (x \in B \et x \in C) \\
			\iff& x \in A \et x \in B \cap C
		\end{align*}
		Donc $(A \cap B) \cap C = A \cap (B \cap C)$ (de même pour $\cup$).

		\bigskip
		\textbf{Loi de Morgan} :

		Soient $E$ un ensemble et $A$ et $B$ deux parties de $E$.

		Soit  $x \in E$.

		\begin{align*}
			x \in \overline{A \cap B} \iff& \text{non} \left( x \in A \et x \in B \right)
				\\
			\iff& x \not\in A \ou x \not\in B \\
			\iff& x \in \overline{A} \ou x \in \overline{B} \\
			\iff& x \in \overline{A} \cup \overline{B}
		\end{align*}

		Donc $\overline{A \cap B} = \overline{A} \cup \overline{B}$
	\end{demo}

	\subsection{Partition d'un ensemble}%
	\label{sub:partition_d_un_ensemble}

	\textbf{Exercice page 4}

	Soit $E = \left\{ 2,3,4,5,6,7,8,9,10,11,12 \right\}$

	$A = \left\{ x \in E  \mid 2 \text{ divise } x \right\} $

	$B = \left\{ x \in E  \mid 3 \text{ divise } x \right\} $

	$C = \left\{ x \in E  \mid x \not\in A \et x \not\in B \right\} $

	\bigskip

	$P_1 = \left\{ A, B, C \right\} $

	$A \cap B \neq \emptyset$ donc $P_1$ n'est pas une partition de $E$.

	\bigskip

	$P_2 = \left\{ A, \overline{A} \right\} $

	$P_2$ est une partition de $E$.

	\bigskip

	$P_3 = \left\{ A, B \setminus A, C \right\} $

	$P_3$ est une partition de $E$.


	\section{Exemples de démonstrations avec les ensembles}%
	\label{sec:exemples_de_demonstrations_avec_les_ensembles}

	\subsection{Prouver l'inclusion $E \subset F$}%
	\label{sub:prouver_l_inclusion_e_subset_f_}

	\textbf{Exercice 1 page 4}

	Soient $E$ un ensemble et $A$, $B$, et $C$ trois parties de $E$.

	On suppose que $A \cup B \subset A \cup C$ \et $A \cap B \subset A \cap C$

	Montrer que $B \subset C$.

	\begin{itemize}
		\item Soit $x \in B$
		\begin{itemize}
			\item 1er cas : $x \in A$

			On en déduit que $x \in A \cap B$

			Or $A \cap B \subset A \cap C$

			Donc $x \in C$

		\item 2e cas : $x \not\in A$

		On a : $x \in A \cup B$

		Or $A \cup B \subset A \cup C \et x \not\in A$

		Donc $x \in C$
		\end{itemize}
		Dans tous les cas $x \in C$
	\end{itemize}
	\Conclusion : $B \subset C$

	\subsection{Comment prouver l'égalité $E = F$}%
	\label{sub:comment_prouver_l_egalite_e_f_}

	\textbf{Exercice 3 page 4}

	Soient $A$ et $B$ deux parties d'un ensemble $E$.

	Montrer que $\overline{A} \Delta \overline{B} = A\Delta B$

	\begin{align*}
		\overline{A} \Delta \overline{B} &= ( \overline{A} \setminus \overline{B}) \cup (
		\overline{B} \setminus \overline{A} )\\
		&= (\overline{A} \cap B) \cup (\overline{B} \cap A)\\
		&= (B \cap \overline{A}) \cup (A \cup \overline{B}) \\
		&= (A \setminus B) \cup (B \setminus A) \\
		&= A \Delta B
	\end{align*}

	\textbf{Exercice 2 page 5}

	Soient $A$ et $B$ deux parties d'un ensemble $E$.

	Montrer que $A \cup B = A \cap B \iff A = B$

	Supposons que $A \cup B = A \cap B$

	\begin{itemize}
		\item $A \subset B$ en effet

		Soit $x \in A$. On a $x \in A \cup B$

		Or $A \cup B = A\cap B$

		Donc $x \in A \cap B$

		Donc  $x \in B$
	\item $B \subset A$ en effet ... (même raisonnement)
	\end{itemize}

	\highlighty{Donc $A = B$}

	Supposons que $A = B$

	 $A \cup B = A \cup A = A$ \et $A \cap B = A \cap A = A$

	 \Conclusion : $A \cup B = A \cap B$
\end{document}
